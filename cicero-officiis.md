**Editio**: M. Winterbottom, Oxonii.

## Liber I

* 1.6, sc. "nisi aut ab iis qui dicant 'honestatem *solam* propter se', aut ab iis qui
'*maxime*'", &c.
* 1.7, "omniane officia perfecta sint" **perfecta**: absoluta, sine
condicione. 
* 1.8, **probabilis**: ὅ τι ἀγαθὸν εἶναι δοκεῖ.
* 1.12, **victus**: non pastus, sed degendae humano cultu vitae modus.
