Commentarii de Libri Antiquorum Graecorum & Romanorum
===

## Latine

Ineuntes salvete.

Hic sunt commentarii mei quoscumque variis de lectionibus scripsi.

## Ἑλληνιστί

χαίρετε.

τάδ’ ἐστιν ὑπ’ ἐμοῦ γεγράμματα περὶ ὧν ἀναγιγνώσκων τυγχάνω βιβλίων ὑπομνήματα.
