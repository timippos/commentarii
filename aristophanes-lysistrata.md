ΑΡΙΣΤΟΦΑΝΟΥΣ ΛΥΣΙΣΤΡΑΤΗ
===

**Editio:** J. Henderson, Oxonii.

* 28-9, **λεπτόν**: vel πράγμα ipsum, vel πέος cuiusdam viri, quod non nisi quassatum erigatur.
* 64, **τἀκάτειον ᾔρετο**: oraculum poscere Hecates, ut solent mulieres dum domu abeant.
* 68, **ἀνάγυρός**: civitas Athenarum, cuius nomen et appellatur herba quaedam turpis fetoris.
